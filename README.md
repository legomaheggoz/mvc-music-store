# MVC Music Store

MVC Music Store is a tuutorial project [originally written](http://www.asp.net/mvc/overview/older-versions/mvc-music-store/mvc-music-store-part-1) by [Jon Galloway](https://twitter.com/jongalloway).

This repo has been updated to MVC 4, with plans to update the code and documentation to MVC 5 as well.

## Building This Book

The documentation is written in [GitBook](https://www.gitbook.io/). To build the documentation yourself run the following commands from the /doc directory:

```
> npm install -g gitbook
> gitbook serve
```

## License
This project is distributed under the terms of the [MS-PL](http://opensource.org/licenses/MS-PL)

- Provides copyright protection: **True**
- Can be used in commercial applications: **True**
- Bug fixes / extensions must be released to the public domain: **False**
- Provides an explicit patent license: **True**
- Can be used in proprietary (closed source) applications: **True**
- Is a viral licence: **False**
- Supported by CodeProject: **True**