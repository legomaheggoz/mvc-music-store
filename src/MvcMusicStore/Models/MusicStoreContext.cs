﻿using System.Data.Entity;

namespace MvcMusicStore.Models
{
    public class MusicStoreContext : DbContext
    {
        public MusicStoreContext()
            : base("musicstoreconnection")
        {
            Database.SetInitializer<MusicStoreContext>(new SeedingInitializer());
        }

        public DbSet<Album> Albums { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
    }
}