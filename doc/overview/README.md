# Overview
The MVC Music Store is a tutorial application that introduces and explains step-by-step how to use ASP.NET MVC and Visual Web Developer for web development. We’ll be starting slowly, so beginner level web development experience is okay.
The application we’ll be building is a simple music store. There are three main parts to the application: shopping, checkout, and administration.

![](image001.jpg)

Visitors can browse Albums by Genre:

![](image002.jpg)

They can view a single album and add it to their cart:

![](image003.jpg)

They can review their cart, removing any items they no longer want:

![](image004.jpg)

Proceeding to Checkout will prompt them to login or register for a user account.

![](image005.jpg)

After creating an account, they can complete the order by filling out shipping and payment information. To keep things simple, we’re running an amazing promotion: everything’s free if they enter promotion code “FREE”!

![](image006.jpg)


![](image007.jpg)

After ordering, they see a simple confirmation screen:

![](image008.jpg)

In addition to customer-faceing pages, we’ll also build an administrator section that shows a list of albums from which Administrators can Create, Edit, and Delete albums:

![](image009.jpg)
