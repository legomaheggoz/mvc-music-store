# About This Book
This book is based on the [ASP.Net MVC Music Store](http://www.asp.net/mvc/overview/older-versions/mvc-music-store/mvc-music-store-part-1) built by John Galloway, updated for MVC 4.
