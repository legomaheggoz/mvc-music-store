## A first look at the Store Manager

Now run the application and browse to /StoreManager/. This displays the Store Manager Index we just modified, showing a list of the albums in the store with links to Edit, Details, and Delete.

![](storeManagerPage.jpg)

Clicking the Details link displays a page with detailed information for an individual Album.

![](detailsPage.jpg)

Click the Back to List link, then click on a Delete link. This displays a confirmation dialog, showing the album details and asking if we’re sure we want to delete it.

![](deletePage.jpg)

Clicking the Delete button at the bottom will delete the album and return you to the Index page, which shows the album deleted.

### Including JavaScript Files for Edit Forms

If you tried clicking on an edit link you would have seen an error page telling you there is a section in your Edit.cshtml page that doesn't have a placeholder in _Layout.cshtml.

![](editPageError.jpg)

Look at the bottom of Edit.cshtml and you will find the following code snippet

```cs
@section Scripts {
    @Scripts.Render("~/bundles/jqueryval")
}
```

ASP.NET MVC Scaffolding assumes it's running inside a layout page similar to the default *_Layout.cshtml* that comes with with project templates othen than the empty project. To make your layout align with the expected layout add a scripts section placeholder to *_Layout.cshtml* 

```cs
<body>
    ...
    @RenderBody()
    @RenderSection("scripts", required: false)
</body>
```

While the page will now load, we're not quite done. We have to add the scripts that the `jqueryval` bundle is referring to. Similar to how we [previously bundled stylesheets](../views_and_viewmodels/including_stylesheet.md) we want to create a bundle for our JavaScript. 

To add the required JavaScript files to our page we'll again use NuGet to download the client side dependencies. Right click on References under the MvcMusicStore project and select Manage NuGet Packages...

The script were are looking for is the Microsoft jQuery Unobtrusive Validation. But because of the way dependencies are defined between packages, if you try to install the Microsoft jQuery Unobtrusive Validation package directly NuGet will throw an exception trying to migrate between the different version that are required for that package and it's dependencies. So instead install the jQuery Validation package (which automatically includes jQuery) **then** install Microsoft jQuery Unobtrusive Validation. Your NuGet package manager dialog should look like this after you're complete:

![](unobtrusiveValidationNuget.jpg)

The NuGet install would have created a Scripts folder in your solution and placed the downloaded JavaScript files in there.

![](jquerySolutionExplorer.jpg)

Lastly, we need to configure a scripts bundle in out *BundleConfig.cs* file, similar to what we did with the style bundle. Add the following to the `RegisterBundles` method

```cs
bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            "~/Scripts/jquery-{version}.js",
            "~/Scripts/jquery.unobtrusive*",
            "~/Scripts/jquery.validate*"));
```

Because JavaScript files can update version relatively quickly, and NuGet makes it easy to incorporate updates, we don't want to take a hardcoded dependency to a single version in our bundle configuration. So we use the `{version}` and `*` placeholders to represent parts of our file names that are liable to change.

The edit page is now fully functional and should be viewable in your browser.

![](editPage.jpg)

We’re not done with the Store Manager, but we have working controller and view code for the CRUD operations to start from.