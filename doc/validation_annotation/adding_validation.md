# Using Data Annotations for Model Validation

We have a major issue with our Create and Edit forms: they’re not doing any validation. We can do things like leave required fields blank or type letters in the Price field, and the first error we’ll see is from the database.

We can easily add validation to our application by adding Data Annotations to our model classes. Data Annotations allow us to describe the rules we want applied to our model properties, and ASP.NET MVC will take care of enforcing them and displaying appropriate messages to our users.

## Adding Validation to our Album Forms

We’ll use the following Data Annotation attributes:

- **Required** – Indicates that the property is a required field
- **DisplayName** – Defines the text we want used on form fields and validation messages
- **StringLength** – Defines a maximum length for a string field
- **Range** – Gives a maximum and minimum value for a numeric field
- **Bind** – Lists fields to exclude or include when binding parameter or form values to model properties
- **ScaffoldColumn** – Allows hiding fields from editor forms

>Note: For more information on Model Validation using Data Annotation attributes, see the MSDN documentation at http://go.microsoft.com/fwlink/?LinkId=159063

Open the Album class and add the following using statements to the top.

```cs
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
```
 
Next, update the properties to add display and validation attributes as shown below.

```cs
namespace MvcMusicStore.Models
{
    public class Album
    {
        [ScaffoldColumn(false)]
        public int AlbumId { get; set; }
        
        [DisplayName("Genre")]
        public int GenreId { get; set; }
        
        [DisplayName("Artist")]
        public int ArtistId { get; set; }
        
        [Required(ErrorMessage = "An Album Title is required")]
        [StringLength(160)]
        public string Title { get; set; }
        
        [Required(ErrorMessage = "Price is required")]
        [Range(0.01, 100.00, ErrorMessage = "Price must be between 0.01 and 100.00")]
        public decimal Price { get; set; }
        
        [DisplayName("Album Art URL")]
        [StringLength(1024)]
        public string AlbumArtUrl { get; set; }
        
        public Genre  Genre    { get; set; }
        public Artist Artist   { get; set; }
    }
}
```

After having added these attributes to our Album model, our Create and Edit screen immediately begin validating fields and using the Display Names we’ve chosen (e.g. Album Art Url instead of AlbumArtUrl). Run the application and browse to /StoreManager/Create.

![](createForm.jpg)