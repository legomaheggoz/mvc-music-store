## Testing the Client-Side Validation

Server-side validation is very important from an application perspective, because users can circumvent client-side validation. However, webpage forms which only implement server-side validation exhibit three significant problems.

1. The user has to wait for the form to be posted, validated on the server, and for the response to be sent to their browser.
2. The user doesn’t get immediate feedback when they correct a field so that it now passes the validation rules.
3. We are wasting server resources to perform validation logic instead of leveraging the user’s browser.


Fortunately, the ASP.NET MVC scaffold templates have client-side validation built in, requiring no additional work whatsoever.

![](validation1.jpg)

As soon as you type something into  the Title field the client side validation picks up the change and the validation message is immediately removed. You can enter 0 in the Price field which corrects the Required validation, but the Range condition will still not be satisfied.

![](validation2.jpg)

