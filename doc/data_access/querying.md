## Querying the Database

Now let’s update our StoreController so that instead of using “dummy data” it instead calls into our database to query all of its information. We’ll start by declaring a field on the StoreController to hold an instance of the MusicStoreEntities class, named storeDB:
```cs
public class StoreController : Controller
{
    MusicStoreContext storeDB = new MusicStoreContext();
    ...
}
```

### Updating the Store Index to query the database

The MusicStoreEntities class is maintained by the Entity Framework and exposes a collection property for each table in our database. Let’s update our StoreController’s Index action to retrieve all Genres in our database. Previously we did this by hard-coding string data. Now we can instead just use the Entity Framework context Generes collection:
```cs
public ActionResult Index()
{
    var genres = storeDB.Genres.ToList();
    return View(genres);
}
```

No changes need to happen to our View template since we’re still returning the same StoreIndexViewModel we returned before - we’re just returning live data from our database now.

When we run the project again and visit the “/Store” URL, we’ll now see a list of all Genres in our database:

![](indexPage.jpg)

### Updating Store Browse and Details to use live data

With the /Store/Browse?genre=[some-genre] action method, we’re searching for a Genre by name. We only expect one result, since we shouldn’t ever have two entries for the same Genre name, and so we can use the .Single() extension in LINQ to query for the appropriate Genre object like this (don’t type this yet):

```cs
var example = storeDB.Genres.Single(g => g.Name == “Disco”);
```

The Single method takes a Lambda expression as a parameter, which specifies that we want a single Genre object such that its name matches the value we’ve defined. In the case above, we are loading a single Genre object with a Name value matching Disco.

We’ll take advantage of an Entity Framework feature that allows us to indicate other related entities we want loaded as well when the Genre object is retrieved. This feature is called Query Result Shaping, and enables us to reduce the number of times we need to access the database to retrieve all of the information we need. We want to pre-fetch the Albums for Genre we retrieve, so we’ll update our query to include from Genres.Include(“Albums”) to indicate that we want related albums as well. This is more efficient, since it will retrieve both our Genre and Album data in a single database request.

With the explanations out of the way, here’s how our updated Browse controller action looks:

```cs
public ActionResult Browse(string genre)
{
    // Retrieve Genre and its Associated Albums from database
    var genreModel = storeDB.Genres.Include("Albums")
        .Single(g => g.Name == genre);
 
    return View(genreModel);
}
```

We can now update the Store Browse View to display the albums which are available in each Genre. Open the view template (found in /Views/Store/Browse.cshtml) and add a bulleted list of Albums as shown below.

```html
@model MvcMusicStore.Models.Genre
@{
    ViewBag.Title = "Browse";
}
<h2>Browsing Genre: @Model.Name</h2>
<ul>
    @foreach (var album in Model.Albums)
    {
        <li>
            @album.Title
        </li>
    }
</ul>
```

Running our application and browsing to /Store/Browse?genre=Jazz shows that our results are now being pulled from the database, displaying all albums in our selected Genre.

![](browsePage.jpg)

We’ll make the same change to our /Store/Details/[id] URL, and replace our dummy data with a database query which loads an Album whose ID matches the parameter value.

```cs
public ActionResult Details(int id)
{
    var album = storeDB.Albums.Find(id);
 
    return View(album);
}
```

Running our application and browsing to /Store/Details/1 shows that our results are now being pulled from the database.

![](detailsPage.jpg)

Now that our Store Details page is set up to display an album by the Album ID, let’s update the Browse view to link to the Details view. We will use `Html.ActionLink`, exactly as we did to link from Store Index to Store Browse at the end of the previous section. The complete source for the Browse view appears below.

```html
@model MvcMusicStore.Models.Genre
@{
    ViewBag.Title = "Browse";
}
<h2>Browsing Genre: @Model.Name</h2>
<ul>
    @foreach (var album in Model.Albums)
    {
        <li>
            @Html.ActionLink(album.Title, "Details", new { id = album.AlbumId })
        </li>
    }
</ul>
```

We’re now able to browse from our Store page to a Genre page, which lists the available albums, and by clicking on an album we can view details for that album.

![](browsePage2.jpg)