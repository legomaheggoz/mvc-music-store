## Adding Links Between Pages

Our /Store URL that lists Genres currently lists the Genre names simply as plain text. Let’s change this so that instead of plain text we instead have the Genre names link to the appropriate /Store/Browse URL, so that clicking on a music genre like “Disco” will navigate to the /Store/Browse?genre=Disco URL. We could update our \Views\Store\Index.cshtml View template to output these links using code like below (don’t type this in - we’re going to improve on it):

```html
<ul>
    @foreach (var genre in Model)
    {
        <li><a href="/Store/Browse?genre=@genre.Name">@genre.Name</a></li>
    }
</ul>
```

That works, but it could lead to trouble later since it relies on a hardcoded string. For instance, if we wanted to rename the Controller, we’d need to search through our code looking for links that need to be updated.

An alternative approach we can use is to take advantage of an HTML Helper method. ASP.NET MVC includes HTML Helper methods which are available from our View template code to perform a variety of common tasks just like this. The `Html.ActionLink()` helper method is a particularly useful one, and makes it easy to build HTML `<a>` links and takes care of annoying details like making sure URL paths are properly URL encoded.

`Html.ActionLink()` has several different overloads to allow specifying as much information as you need for your links. In the simplest case, you’ll supply just the link text and the Action method to go to when the hyperlink is clicked on the client. For example, we can link to “/Store/” `Index()` method on the Store Details page with the link text “Go to the Store Index” using the following call:

```cs
@Html.ActionLink("Go to the Store Index", "Index")
```

*Note: In this case, we didn’t need to specify the controller name because we’re just linking to another action within the same controller that’s rendering the current view.*

Our links to the Browse page will need to pass a parameter, though, so we’ll use another overload of the `Html.ActionLink` method that takes three parameters:

1. Link text, which will display the Genre name
1. Controller action name (Browse)
1. Route parameter values, specifying both the name (Genre) and the value (Genre name)

Putting that all together, here’s how we’ll write those links to the Store Index view:

```html
<ul>
    @foreach (var genre in Model)
    {
        <li>@Html.ActionLink(genre.Name, "Browse", new { genre = genre.Name })</li>
    }
</ul>
```

Now when we run our project again and access the /Store/ URL we will see a list of genres. Each genre is a hyperlink – when clicked it will take us to our /Store/Browse?genre=[genre] URL.

![](indexPageLinks.jpg)

The HTML for the genre list looks like this:

```html
<ul>
    <li><a href="/Store/Browse?genre=Disco">Disco</a></li>
    <li><a href="/Store/Browse?genre=Jazz">Jazz</a></li>
    <li><a href="/Store/Browse?genre=Rock">Rock</a></li>
</ul>
```