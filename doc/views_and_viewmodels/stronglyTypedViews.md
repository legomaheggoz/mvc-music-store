## Using a Model to pass information to our View

A View template that just displays hardcoded HTML isn’t going to make a very interesting web site. To create a dynamic web site, we’ll instead want to pass information from our controller actions to our view templates.

In the Model-View-Controller pattern, the term Model refers to objects which represent the data in the application. Often, model objects correspond to tables in your database, but they don’t have to.

Controller action methods which return an ActionResult can pass a model object to the view. This allows a Controller to cleanly package up all the information needed to generate a response, and then pass this information off to a View template to use to generate the appropriate HTML response. This is easiest to understand by seeing it in action, so let’s get started.

First we’ll create some Model classes to represent Genres and Albums within our store. Let’s start by creating a Genre class. Right-click the “Models” folder within your project, choose the “Add Class” option, and name the file “Genre.cs”.

![](addClass.jpg)

Then add a public string Name property to the class that was created:

```cs
public class Genre
{
    public string Name { get; set; }
}
```

> Note: In case you're wondering, the { get; set; } notation is making use of C#'s auto-implemented properties feature. This gives us the benefits of a property without requiring us to declare a backing field.

Next, follow the same steps to create an Album class (named Album.cs) that has a Title and a Genre property:

```cs
public class Album
{
    public string Title { get; set; }
    public Genre Genre { get; set; }
}
```

Now we can modify the StoreController to use Views which display dynamic information from our Model. If - for demonstration purposes right now - we named our Albums based on the request ID, we could display that information as in the view below.

![](detailsPage.jpg)

We’ll start by changing the Store Details action so it shows the information for a single album. Add a “using” statement to the top of the StoreControllers class to include the MvcMusicStore.Models namespace, so we don’t need to type MvcMusicStore.Models.Album every time we want to use the album class. The “usings” section of that class should now appear as below.

```cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcMusicStore.Models;
```

Next, we’ll update the Details controller action so that it returns an ActionResult rather than a string, as we did with the HomeController’s Index method.

```cs
public ActionResult Details(int id)
```

Now we can modify the logic to return an Album object to the view. Later in this tutorial we will be retrieving the data from a database – but for right now we will use "dummy data" to get started.

```cs
public ActionResult Details(int id)
{
    var album = new Album { Title = "Album " + id };
    return View(album);
}
```

> Note: If you’re unfamiliar with C#, you may assume that using var means that our album variable is late-bound. That’s not correct – the C# compiler is using type-inference based on what we’re assigning to the variable to determine that album is of type Album and compiling the local album variable as an Album type, so we get compile-time checking and Visual Studio code-editor support.

Let’s now create a View template that uses our Album to generate an HTML response. Before we do that we need to build the project so that the Add View dialog knows about our newly created Album class. You can build the project by selecting the Debug -> Build MvcMusicStore menu item (for extra credit, you can use the Ctrl-Shift-B shortcut to build the project).

![](buildSolution.jpg)

Now that we've set up our supporting classes, we're ready to build our View template. Right-click within the Details method and select “Add View…” from the context menu.

![](addDetailsView.jpg)

We are going to create a new View template like we did before with the HomeController. Because we are creating it from the StoreController it will by default be generated in a \Views\Store\Index.cshtml file.

Unlike before, we are going to check the “Create a strongly-typed” view checkbox. We are then going to select our “Album” class within the “View data-class” drop-downlist. This will cause the “Add View” dialog to create a View template that expects that an Album object will be passed to it to use.

![](addDetailsViewDialog.jpg)

When we click the “Add” button our \Views\Store\Details.cshtml View template will be created, containing the following code.

```cs
@model MvcMusicStore.Models.Album
@{
    ViewBag.Title = "Details";
}
<h2>Details</h2>
```

Notice the first line, which indicates that this view is strongly-typed to our Album class. The Razor view engine understands that it has been passed an Album object, so we can easily access model properties and even have the benefit of IntelliSense in the Visual Studio editor.

Update the `<h2>` tag so it displays the Album’s Title property by modifying that line to appear as follows.

```html
<h2>Album: @Model.Title</h2>
```

Notice that IntelliSense is triggered when you enter the period after the @Model keyword, showing the properties and methods that the Album class supports.

![](razorIntellisense.jpg)

Let's now re-run our project and visit the /Store/Details/5 URL. We'll see details of an Album like below.

![](detailsPage.jpg)

Now we’ll make a similar update for the Store Browse action method. Update the method so it returns an ActionResult, and modify the method logic so it creates a new Genre object and returns it to the View.

```cs
public ActionResult Browse(string genre)
{
    var genreModel = new Genre { Name = genre };
    return View(genreModel);
}
```

Right-click in the Browse method and select “Add View…” from the context menu, then add a View that is strongly-typed add a strongly typed to the Genre class.

![](addBrowseViewDialog.jpg)

Update the `<h2>` element in the view code (in /Views/Store/Browse.cshtml) to display the Genre information.

```html
@model MvcMusicStore.Models.Genre
@{
    ViewBag.Title = "Browse";
}
<h2>Browsing Genre: @Model.Name</h2>
```

Now let’s re-run our project and browse to the /Store/Browse?Genre=Disco URL. We’ll see the Browse page displayed like below.

![](browsePage.jpg)

Finally, let’s make a slightly more complex update to the Store Index action method and view to display a list of all the Genres in our store. We’ll do that by using a List of Genres as our model object, rather than just a single Genre.

```cs
public ActionResult Index()
{
    var genres = new List<Genre>
    {
        new Genre { Name = "Disco"},
        new Genre { Name = "Jazz"},
        new Genre { Name = "Rock"}
    };
    return View(genres);
}
```

Right-click in the Store Index action method and select Add View as before, select Genre as the Model class, and press the Add button.

![](addIndexViewDialog.jpg)

This tells the Razor view engine that it will be working with a model object that can hold several Genre objects. We’re using an IEnumerable<Genre> rather than a List<Genre> since it’s more generic, allowing us to change our model type later to any object type that supports the IEnumerable interface.

Next, we’ll loop through the Genre objects in the model as shown in the completed view code below.

```html
@model IEnumerable<MvcMusicStore.Models.Genre>
@{
    ViewBag.Title = "Store";
}
<h3>Browse Genres</h3>
<p>
    Select from @Model.Count() genres:</p>
<ul>
    @foreach (var genre in Model)
    {
        <li>@genre.Name</li>
    }
</ul>
```

Notice that we have full IntelliSense support as we enter this code, so that when we type “@Model.” we see all methods and properties supported by an IEnumerable of type Genre.

![](ienumerableIntellisense.jpg)

Within our “foreach” loop, Visual Web Developer knows that each item is of type Genre, so we see IntelliSence for each the Genre type.

![](foreachIntellisense.jpg)

When we run the application and browse to /Store, we see that both the count and list of Genres is displayed.

![](indexPage.jpg)