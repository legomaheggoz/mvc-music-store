# Membership and Authorization

Our Store Manager controller is currently accessible to anyone visiting our site. Let's change this to restrict permission to site administrators.

## Adding the Account Controller, Views, and Models

One difference between the full ASP.NET MVC 4 Internet Application template and the ASP.NET MVC 4 Empty Web Application template is that the empty template doesn't include authentication/authorization. The Internet Application template includes a new feature for ASP.NET MVC 4 called SimpleMembership. It is an EntityFramework based membership system that includes support for third-party logins with OAuth. We'll incorporate the SimpleMembership system into our empty template by adding some of the files generated by the Internet Application template, and along the way fix some of the [known issues with SimpleMembership](http://odetocode.com/blogs/scott/archive/2012/09/24/perils-of-the-mvc4-accountcontroller.aspx).

### Preparing Dependencies

Before we add any code we need to add some dependencies that the SimpleMembership system depends on. Add the Microsoft WebPages OAuth library by entering the following command at the package manager console (Tools > NuGet Package Manager > Package Manager Console).

```
Install-Package Microsoft.AspNet.WebPages.OAuth -Version 2.0.30506
```

Then we need to add the .Net framework `System.Transactions` assembly for database transactions. Right click on References in the MvcMusicStore project and select Add Reference...

![](addReference.jpg)

Then under Assemblies > Framework, tick the box next to System.Transactions and click OK.

![](referenceSystemTransactions.jpg)

### Adding Prepared Files

Because other project templates include the full set of account management classes by default we won't spend much time detailing them. Instead, they'll come from the [example sourcecode](https://bitbucket.org/pnewhook/mvc-music-store).

#### Account Models

Copy AccountModels.cs file from the example into your own Models folder.

This file includes the `UserProfile` class, which represents the user that will be persisted to your database. But it also includes a number of other classes designed specifically for strongly typed views. These types of models have a specific name, view-models. View-models are representations of model classes specifically for consumption by views. They have additional fields to display view specific data, and crutially, don't include fields that should never be represented in a view (secure values and administrative fields).

#### Account Controller

From the same example source code, copy  *AccountController.cs* into your Controllers folder

#### Account Views

Finally you can add the views that will provide the interface for logging in and out. Copy the entire Account folder out of the example code into your views folder.

![](copyViews.jpg)

```cs
using System.Data.Entity;

namespace MvcMusicStore.Models
{
    public class MusicStoreContext : DbContext
    {
        public MusicStoreContext()
            : base("musicstoreconnection")
        {
            Database.SetInitializer<MusicStoreContext>(new SeedingInitializer());
        }

        public DbSet<Album> Albums { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
    }
}
```
#### Initializing Web Security

The default templates using SimpleMembership include a an ASP.NET MVC filter that initializes the authentication system. However, as noted earlier, this is a source of pain with SimpleMembership. It's better to initialize the the security framework at application startup. Copy from the example code or create a class *WebSecurityConfig* with the following content and put it in the App_Start folder.

```cs
using System;
using System.Data.Entity.Infrastructure;
using MvcMusicStore.Models;
using WebMatrix.WebData;

namespace MvcMusicStore
{
    public class WebSecurityConfig
    {
        public static void InitializeWebSecurity()
        {
            try
            {
                using (var context = new MusicStoreContext())
                {
                    if (!context.Database.Exists())
                    {
                        // Create the SimpleMembership database without Entity Framework migration schema
                        ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                    }
                }

                WebSecurity.InitializeDatabaseConnection("musicstoreconnection", 
                "UserProfile", "UserId", "UserName", autoCreateTables: true);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("The ASP.NET Simple Membership
                database could not be initialized. For more information, please see
                http://go.microsoft.com/fwlink/?LinkId=256588", ex);
            }
        }
    }
}
```

We want to let Entity Framework create the database for us before , because it compares the DbContext against the database to determine if it needs to update the database. Because `WebSecurity.InitializeDatabaseConnection` creates account tables, EF will always think the database doesn't match the DbContext because it doesn't manage the additional tables. So create or copy the class MusicStoreDbContextInitializer in the App_Start Folder.

```cs
using MvcMusicStore.Models;
using WebMatrix.WebData;

namespace MvcMusicStore
{
    public class MusicStoreDbContextInitializer
    {
        public static void InitializeDbContext()
        {
            var context = new MusicStoreContext();
            context.Database.Initialize(true);
            if (!WebSecurity.Initialized)
            {
                WebSecurity.InitializeDatabaseConnection("musicstoreconnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);
            }
        }
    }
}
```

And call `InitializeDbContext` from your `Applicaiton_Start` method in *Global.asax.cs*.

```cs
protected void Application_Start()
{
    AreaRegistration.RegisterAllAreas();

    WebApiConfig.Register(GlobalConfiguration.Configuration);
    FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
    RouteConfig.RegisterRoutes(RouteTable.Routes);
    BundleConfig.RegisterBundles(BundleTable.Bundles);
    MusicStoreDbContextInitializer.InitializeDbContext();
}
```

The updated solution should look like the following:

![](accountSolutionExplorer.jpg)

> If all this extra setup to configure authentication seems heavy-handed to you, you're not alone. Once an application is up and runnning, the SimpleMembership system is a good security provider, but it doesn't play well with Entity Framework when creating and updating the database. This issues is partly resolved by [Code first migrations](http://msdn.microsoft.com/en-us/data/jj591621.aspx) which is a tool to update databases as your model changes. Code First Migrations are beyond the scope of this tutorial, but they are a helpful tool to learn. 

> Recent versions of ASP.NET MVC dropped SimpleMembership in favour of a more integrated security system called ASP.NET Identity. It is a better choice for new projects, but requires .Net 4.5.

### Adding UserProfile to your DbContext

The default templates put the `UserProfile` class in it's own `DbContext` which is helpful for the template, but brings a host of problem in a real applicaiton. Modify your `MusicStoreContext` class to include a `DbSet<UserProfile>`.

```cs
public class MusicStoreContext : DbContext
{
    ...
    public DbSet<Artist> Artists { get; set; }
    public DbSet<UserProfile> UserProfiles { get; set; }
}
```

### Defining the Login Page in web.config

If a user tries to access a secured page without being logged in, we want to redirect them to the login screen. Add the following to your *web.config* inside the `system.web` element.

```xml
<system.web>
    <httpRuntime />
    <compilation debug="true" targetFramework="4.0" />
    <authentication mode="Forms">
      <forms loginUrl="~/Account/Login" timeout="2880" />
    </authentication>
```

### Testing Role Based Authentication

We'll dig into more about authorizing pages in a later chapter, but suffice to say for now, we can use the `[Authorize]` filter to limit access to certain action methods or even entire controllers.

We can also limit the same resources to not just a logged in users, but specific groups of users, or *roles*. Modify `InitializeWebSecurity` by creating a new 'admin' role.

```cs
WebSecurity.InitializeDatabaseConnection("musicstoreconnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);

if (!Roles.RoleExists("admin"))
{
    WebSecurity.CreateUserAndAccount("administrator", "password1");
    Roles.CreateRole("admin");
    Roles.AddUserToRole("administrator", "admin");
}
```

Now we can add the authorize attribute to your `StoreController` class. `StoreManagerController` takes an optional `Roles` parameter to specify which roles should be allowed access. If the `Roles` parameter is omitted, all authenticate users may view the page.

```cs
[Authorize(Roles = "admin")]
public class StoreManagerController : Controller
{
    ...
}
```

Now when you visit /StoreManager you will be redirected to the login page. 
 
![](authenticationRedirect.jpg)

Notice the *ReturnUrl* paramater that was automatically added to the URL. This parameter is used in the Login action method to redirect you to the page you initially requested when you were sent to the login page, in this case /StoreManager

![](storeManagerPage.jpg)