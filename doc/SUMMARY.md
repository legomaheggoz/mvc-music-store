# Summary

* [Title](README.md)
* [About This Book](about_this_book/README.md)
* [Overview](overview/README.md)
* [File > New Project](file__new_project/installing_the_software.md)
   * [Installing the Software](file__new_project/installing_the_software.md)
   * [Creating a new ASP.NET MVC 4 project](file__new_project/creating_a_new_aspnet_mvc_4_project.md)
* [Controllers](controllers/README.md)
   * [Adding a Home Controller](controllers/adding_a_home_controller.md)
   * [Running the Application](controllers/running_the_application.md)
   * [Adding a StoreController](controllers/adding_a_storecontroller.md)
* [Views and Models](views_and_viewmodels/README.md)
   * [Adding a View Template](views_and_viewmodels/adding_a_view_template.md)
   * [Using a Layout for Common Site Elements](views_and_viewmodels/common_site_elements.md)
   * [Including StyleSheets and JavaScript Using Bundles](views_and_viewmodels/including_stylesheet.md)
   * [Using a Model to pass information to our View](views_and_viewmodels/stronglyTypedViews.md)
   * [Adding Links Between Pages](views_and_viewmodels/adding_links.md)
* [Models and Data Access](data_access/README.md)
   * [Installing Entity Framework Dependencies](data_access/efDependencies.md)
   * [Database access with Entity Framework Code-First](data_access/accessing_the_database.md)
   * [Querying the Database](data_access/querying.md)
* [Edit Forms and Templating](forms_templating/storemanagercontroller.md)
   * [Creating the StoreManagerController](forms_templating/storemanagercontroller.md)
   * [Modifying a Scaffolded View](forms_templating/modifying_scaffolding.md)
   * [A First Look at the Store Manager](forms_templating/first_look_store_manager.md)
   * [Looking at the Store Manager Controller code](forms_templating/storemanagercontroller_code.md)
* [Using Data Annotations for Model Validation](validation_annotation/adding_validation.md)
   * [Adding Validation to our Album Forms](validation_annotation/adding_validation.md)
   * [Client Side Validation](validation_annotation/client_side_validation.md)
* [Membership and Authorization](membership_authorization/adding_accounts.md)
   * [Adding the AccountController, Views, and Models](membership_authorization/adding_accounts.md)


